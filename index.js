const link = "https://api.ipify.org/?format=json"
const infoBlock = document.querySelector(".info-block")
const getIpBtn = document.getElementById('get-ip-btn')

async function getIp(){
    const response = await fetch(link)
    const userIp = await response.json()
    return userIp.ip
}

async function getInfoByIp(){
    const userIp = await getIp()
    const infoResponse = await fetch(`http://ip-api.com/json/${userIp}?fields=continent,country,city,regionName`)
    const info = await infoResponse.json()
    const {continent,country,city,regionName} = info
    const infoHtml = `<li>Continent : ${continent}</li>
                    <li>Country : ${country}</li>
                    <li>City : ${city}</li>
                    <li>RegionName : ${regionName}</li>`
    infoBlock.innerHTML = infoHtml
}

getIpBtn.addEventListener('click', getInfoByIp)